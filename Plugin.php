<?php

/**
 * Plugin Name: Plugin 
 * Author: Sorena
 * Plugin URI: https://example.com
 * Version: 1.00
 * Description: It just a normal plugin
 * License: GPLv2 or later
 * Text Domain: plugin
*/


// For security if someone comes directly to plugin
defined ('ABSPATH') or die ('Who are u man..!?');


// Constant variables
define( 'PLUGIN_PATH', plugin_dir_path( __FILE__ ));
define( 'INCLUDES', plugin_dir_path(__FILE__) . '/includes');
define( 'PLUGIN_URL', plugin_dir_url( dirname( __FILE__ )) . 'plugin');


// Active plugin
function activate_fauth() {
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'activate_fauth' );

// Deactive plugin
function deactivate_fauth() {
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'deactivate_fauth' );

// Include all services
function startPlugin() {
    // Add shortcodes
    require_once( INCLUDES . "/Base/Shortcodes.php" );

    // Enqueue scripts
    require_once( INCLUDES . "/Base/Enqueue.php" );

    // Admin page
    require_once( INCLUDES . "/Admin.php" );
}
startPlugin();