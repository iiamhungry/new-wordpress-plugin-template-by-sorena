# Wordpress plugin template
Wordpress plugin structured base functional programmering.

## Installation

Use git clone to import repository.

### Use https
```bash
git clone https://gitlab.com/iiamhungry/new-wordpress-plugin-template-by-sorena.git
```
### Use ssh
```bash
git clone git@gitlab.com:iiamhungry/new-wordpress-plugin-template-by-sorena.git
```

## Usage
Just after clone change plugin word with your plugin name.

## Explanation
We have 3 folders into plugin directory plus one file that necessary for plugin devlopment.

### Plugin name file 
We place plugin information at first of this file and active, deactive hook plus startPlugin method that include others files to need load in plugin.
Also we define constans in this file too.

### Assets
This folder is for keep our JS and CSS files.

### Includes
This folder is for include necessary file that has two subfolder and one file.
First folder is API that we keep wordpress routing and ajax requests threr.
The other folder is BASE that almost other files keep there like shortcodes and enqueue scripts.
The last is Admin.php that is for admin page and other admin stuff :\.

### Templates
We keep template files here like admin page html codes or shortcodes and include them from here.



Enjoy developing wordpress plugin.