<?php

// Ajax request from front
add_action( 'wp_ajax_callbackFunction', 'callbackFunction' );
add_action( 'wp_ajax_nopriv_callbackFunction', 'callbackFunction' );

function callbackFunction() {
    if( !DOING_AJAX ) {
        wp_die();
    }

    try {
        // Do stuff ...

        $response = [
            'ok' => true,
        ];
    
        wp_send_json( $response );
        wp_die()

    } catch( Exception $err ) {
        // Handel errors ... 

        ;
        $response = [
            'ok' => false,
            'error' => $err->message
        ];

        wp_send_json( $response );
        wp_die();
    }
}