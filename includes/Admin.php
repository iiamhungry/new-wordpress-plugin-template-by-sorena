<?php

// Add page
add_action( 'admin_menu', function() {
    add_menu_page( 
        'Page_title', 
        'Menu_title', 
        'Capability', // Mostly equal to manage_options
        'Menu_slug', 
        fn() => require_once( PLUGIN_PATH . "/templates/admin.php" ), // Callback
        'Icon_url',
        'Menu_position'
    ); 
});