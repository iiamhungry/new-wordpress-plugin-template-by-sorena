<?php

// Enqueue scripts for front
add_action( 'wp_enqueue_scripts', function() {
    // Enqueue script and style
    wp_enqueue_style( 'pluginstyle', PLUGIN_URL . '/assets/pluginCss.css' );
    wp_enqueue_script( 'pluginscript', PLUGIN_URL . '/assets/pluginJs.js', ['jquery'] );
    
    // Defining a variable that store path of admin-ajax.php for ajax requests
    wp_localize_script( 'myscript', 'urls', [ 
        'ajax_url' => admin_url( 'admin-ajax.php'),
    ]);
});